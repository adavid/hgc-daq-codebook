# HGC Fast Command Codebook Candidates

## Scope/Strategy

Construction of codebooks and codes-command for the CMS HGCAL Fast Control.
The task is to choose 32 commands (see below) among all possible 8-bits.


### List of commands

Command --- expected Frequency [Hz] --- **Type**

* kCode: 38.0E+6 **Idle**
* L1A: 1.0E+6 **High frequency**
* PreL1A:			1.0E+6 **High frequency**
* L1A+PreL1A:		25.0E+3 **High frequency**
* OrbitSync:			11.2E+3 **Destructive code**
* OrbitSync+L1A:		280.6E+0
* OrbitSync+PreL1A:		280.6E+0
* OrbitSync+L1A+PreL1A:	7.0E+0
* NZS+L1A:		10.0E+0
* NZS+L1A+PreL1A:	250.0E-3
* CalPulseInt:	10.0E+0 **Destructive**
* CalPulseInt+L1A:		250.0E-3 **Destructive**
* CalPulseInt+PreL1A:		250.0E-3 **Destructive**
* CalPulseExt:			10.0E+0 **Destructive**
* CalPulseExt:	L1A		250.0E-3 **Destructive**
* CalPulseExt:	PreL1A		250.0E-3 **Destructive**
* OrbitCounterReset+OrbitSync:		10.0E-3 
* EventCounterReset:		10.0E-3 
* EventBufferClear: 10.0E-3
* LinkResetROCT: 10.0E-3 **Destructive**
* LinkResetROCD: 10.0E-3 **Destructive**
* LinkResetECONT: 10.0E-3 **Destructive**
* LinkResetECOND: 10.0E-3 **Destructive**
* ChipSync:	10.0E-3 
* CalPulseInt+L1A+PreL1A:	6.3E-3
* CalPulseExt+L1A+PreL1A:	6.3E-3
* Spare-1: ?
* Spare-2: ?		
* Spare-3: ?		
* Spare-4: ?		
* Spare-5: ?		
* Spare-6: ?		

See [this spreadsheet](https://docs.google.com/spreadsheets/d/1tJ6jy9zY0_Fhr_WKaKF9yIxai9-_wcVbd4EMeB95Iz4/edit?usp=sharing) for the list of commands.


## Strategy
The codebook is constructed pursuing the following strategy.

### Constructing of all possible codebooks
Find all possible IDLE codes (=kCode) and potential codes. This has been implemented already in [this notebook by A. David](https://cernbox.cern.ch/index.php/s/KDP6m4WeegGyriO).
In summary: 

* Among all 8-bit words (N=256), only the 70 DC-balanced codes (equal number of 0's and 1's) are selected and can be used for making this codebook.  
* The candidates for the IDLE code, that occurs at the highest frequency, are identified. Those must be chosen such that it is possible to retrieve the 40MHz clock phase in the idle stream. In practice, this implies that in the stream of two consecutively appearing IDLE words, the IDLE word is found exactly twice.
* For a given IDLE code, the subset of the balanced codes is constructed whose entries do not reproduce the IDLE code when combined as pairs.

**At this point, there is a list of potential IDLE codes with unordered sets of possible codes assigned to them. The latter defines a codebook for a given IDLE code.**

### Codes - command assignment
The assignment of the potential codes to the commands is the original contribution of this work.

The key metric is the [hamming distance](https://en.wikipedia.org/wiki/Hamming_distance) (**HD**) which measures the number of bitflips to convert a given bitstream (number) into another.

__Guidelines as discussed with A. David on 18 March 2020:__

* **High frequency** commands, such as the L1A, should have codes with maximum HDs to the **IDLE** and maximum HD with respect to each other.
* **Destructive** commands should have codes with maximum HD to the **High frequency** command codes.
* **Destructive** commands should have minimum HD to each other.

__General guidelines:__

* The HD of all codes to the **IDLE** should be as large as possible. In practice, this implies that the HD should be larger than 2.
* The cardinality of each "category" (see below) is maximised sequentially.
* The number of pairs with ```HD=2``` is ultimately minimised.


__Applied procedure:__

1. **"Category 1:"** Find all codes in codebook with ```HD==6``` to the **IDLE** code. Construct all subsets of cardinality 3 among those whose entries have HD>=4 to each other. Each of these subsets defines a **category 1 candidate**. The entries therein will be assigned to the **High frequency** codes.
2. **"Category 2:"** For each category 1 candidate, identify all remaining codes that have ```HD>=4``` to the **IDLE** and to the category 1 codes. The identified codes are considered category 2 qualifiers. Among those, keep the ones that have ```HD==2``` to at least two other category 2 qualifiers. The hereby identified codes will be assigned to **destructive commands**.
They extend the **category 1 candidates** to **category 2 candidates** of which only the ones with highest cardinality are kept.
3. **"Category 3:"** For each category 2 candidate, identify and all add remaining codes that have ```HD>=4`` to the IDLE as well as to the first two (=highest frequency) **category 1** codes. Then, they extend the **category 2 candidates** to **category 3 candidates** of which only the ones with highest cardinality are kept.
4. **"Category 4:"** For each category 3 candidate, identify and all add remaining codes that have ```HD>=4``` to the IDLE and to one of the two first (=highest frequency) **category 1** codes. The hereby identified codes extend the **category 3 candidates** to **category 4 candidates** of which only the ones with highest cardinality are kept.

------------------
It is found empirically that the cardinality of the **category 4 candidates** is greater than the required 32. In other words, this procedure identifies more than 32 codes to match to the commands. **The procedure can be terminated here.**


## Running this code
### Software dependencies
* **Python 2.7**
* **numpy** general purpose utilities([website](https://numpy.org))
* **networkx** for graph-based computations ([website](https://networkx.github.io))
* **pandas** for dataframe-based analysis ([installation](https://pandas.pydata.org))
* **seaborn** statistical data visualisation ([website](https://seaborn.pydata.org))
* **matplotlib** for state-of-the-art data visualisation ([website](https://matplotlib.org))
* **tqdm** pretty printing of loop iterations similar to a progress bar ([github](https://github.com/tqdm/tqdm))
* **tabulate** pretty io of tabular data ([website](https://pypi.org/project/tabulate/))

### Structure
* ```hgc_daq_codebook.py```: Constructs the codebooks and the code - category assignments.
* ```visualise_code.py```: Visualises the hamming distances between all commands in the constructed codebooks. 
* ```utility.py```: Utility functionality such as computation of the hamming distance.
* ```latex/```: Latex templates.

### Execution sequence to reproduce the result
In the repository's directory, execute the following:

* ```python hgc_daq_codebook.py --codeBase balancedCodes```: Construction of codebooks and categories following the procedure as outlined above. The underlying set is the set of all DC-balanced codes.
* ```python visualise_codes.py --codeBase balancedCodes```: Visualises the HD's between the codes for all constructed possibilities. Codes are assigned to command names according to the mapping defined in ```codes/codebook_v1.py```. Also creates a ```.tex``` file for compilation.
* (Optional) ```pdflatex -output-directory=results results/balancedCodes.tex```: Compiles a pdf with distance visualisations for each possibility.


## Preliminary result (20 March 2020)
Following the procedure outlined above, the optimal code-category assignment is the following:
![](img/code_categories_20March2020.png)

The suggested code-command assignment is done by hand and can be found in ```codes/codebook_v1.py```.
The hamming distances are as follows:
![](img/HD_codes_20March2020.png)


## Work in progress (20 March 2020)
- [x] Finalise README.
- [x] ___Preliminary choice of codebook___.
- [x] __Assignment of categories<-->commands__.
- [ ] Error rate estimation.
- [ ] CI .