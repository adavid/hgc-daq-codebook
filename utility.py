#original author: Andre David, andre.david@cern.ch
#adapted by Thorben Quast, thorben.quast@cern.ch
#last modified: 20 March 2020


from collections import Counter
from itertools import product
import re    

def disparity(code):
    return code.count('1')-code.count('0')

def countKCodeInString(kCode, string):
	return len(re.findall('(?=%s)'%kCode, string))

def isKCodeInString(kCode, string):
    return countKCodeInString(kCode, string) != 0

def invertCode(code):
	return code.replace("0", "2").replace("1", "0").replace("2", "1")

def countKCodeInKCodePairs(kCode):
    # In the normal idle stream expect to find the kCode the two times it is there 
    direct = countKCodeInString(kCode, 2*kCode)
    # In the inverted idle stream, we require that it is not present at all
    inverted = countKCodeInString(kCode, 2*invertCode(kCode))
    return (direct, inverted)

def isKCodeGood(kCode):
    (direct, inverted) = countKCodeInKCodePairs(kCode)
    BooleanToStatus = {True: "OK", False: "X"}
    return BooleanToStatus[direct == 2 and inverted == 0]


def hamming_distance(code1, code2):
	return sum(c1 != c2 for c1, c2 in zip(code1, code2))

#The choice of kCode determines which other codes can be used to encode commands, since no consecutive pair of codes can mimic the kCode. This defines a codebook.
def removeCollisions(codes, kCode):
    allPairs = product(codes, repeat=2) 
    collisions = [ pair for pair in allPairs if isKCodeInString( kCode, ''.join(pair) ) ]
    
    if not collisions:
        return codes

    rows, columns = zip(*collisions)
    counts = Counter(rows)+Counter(columns)
    biggestOffender, bigOffHits = counts.most_common(1)[0]

    codes.remove(biggestOffender)

    return removeCollisions(codes, kCode)