#Author: Thorben Quast, thorben.quast@cern.ch
#last modified: 20 March 2020
#Visualises the hamming distances between all commands in the constructed codebooks. 


from codes.codebook_v1 import commands_to_codes
from utility import *

import os, sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pylab as plt
import networkx as nx
from itertools import product
import pandas as pd
import seaborn as sns

from tqdm import tqdm


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--codeBase", type=str, help="select codes either from set of valid kCodes or all balanced dodes", default="kCodes", required=False)
parser.add_argument("--outputdir", type=str, help="directory to the output figures", default="results", required=False)
args = parser.parse_args()

if args.codeBase=="kCodes":
	categories = {
		0: (0, 1),
		1: (1, 4),
		2: (4, 11),
		3: (11, 18),
		4: (18, 28)
	}
elif args.codeBase=="balancedCodes":
	categories = {
		0: (0, 1),
		1: (1, 4),
		2: (4, 17),
		3: (17, 24),
		4: (24, 32)
	}
else:
	import sys
	sys.exit("No valid codeBase!")

colors = {
	0: "cornflowerblue",
	1: "lime",
	2: "orange",
	3: "red",
	4: "black"
}

#prepare output directory
output_dir = os.path.abspath(args.outputdir)
if not os.path.exists(output_dir):
	os.mkdir(output_dir)

#read input file from before
input_filepath = "results/codeBook_from_%s.txt"%args.codeBase
print "Input file:", input_filepath
inputfile = open(input_filepath, "r")
codeBook = np.genfromtxt(inputfile, skip_header=3)

N_permutations = len(codeBook)
figure_paths = {}

for perm_index in tqdm(range(N_permutations), unit="permutation"):
	figure_paths[perm_index] = {}
	nodes = codeBook[perm_index]
	nodes = np.delete(nodes, 0)		#delete the index
	nodes = nodes.astype(int)

	#compute distances as graphs
	for fig_index, (cat1, cat2) in enumerate([(0, 1), (0, 2), (0,3), (0, 4), (1, 2)]):
		if cat1>=cat2:
			continue

		nodes1 = nodes[categories[cat1][0]:categories[cat1][1]]
		nodes2 = nodes[categories[cat2][0]:categories[cat2][1]]

		allnodes = np.append(nodes1, nodes2)

		G = nx.Graph()
		G.add_nodes_from(allnodes)
		for (c1, c2) in product(allnodes, repeat=2):
		    if c1 < c2: # so as to not add zero-distance useless edges
		        HD = hamming_distance('{:08b}'.format(int(c1)), '{:08b}'.format(int(c2)))
		        G.add_edge(int(c1), int(c2), weight=HD)

		weights = nx.get_edge_attributes(G,'weight')


		layouts = {	
			"circular_layout": nx.circular_layout,
			#"kamada_kawai_layout": nx.kamada_kawai_layout,
			#"random_layout": nx.random_layout,
			#"rescale_layout": nx.rescale_layout,
			#"shell_layout": nx.shell_layout,
			#"spring_layout": nx.spring_layout,
			#"spectral_layout": nx.spectral_layout
		}

		for layout in layouts:
			pos = layouts[layout](G) 
			nx.draw_networkx(G, pos, nodelist=nodes1.tolist(), node_color=("white" if colors[cat1]=="black" else colors[cat1]), node_size=500)
			nx.draw_networkx(G, pos, nodelist=nodes2.tolist(), node_color=("white" if colors[cat2]=="black" else colors[cat2]), node_size=500)
			plt.plot([0],[0], label="Cat. %i, N=%i"%(cat1, categories[cat1][1]-categories[cat1][0]), color=colors[cat1])
			plt.plot([0],[0], label="Cat. %i, N=%i"%(cat2, categories[cat2][1]-categories[cat2][0]), color=colors[cat2])
			_ = nx.draw_networkx_edge_labels(G, pos, edge_labels = weights)
			plt.legend(bbox_to_anchor=(0.15, 0.05))
			plt.title("%s: Option %i/%i" % (args.codeBase, 1+perm_index, N_permutations))
			fig_path = os.path.abspath("%s/%s_perm_%i_cat%i_vs_cat%i.pdf"%(output_dir, args.codeBase, 1+perm_index, cat1, cat2))
			figure_paths[perm_index][fig_index] = fig_path
			plt.savefig(fig_path)
			plt.clf()


	#2. compute distances as heat map
	distances = []
	for d1_i in range(len(nodes)):
		c1 = nodes[d1_i]
		for d2_i in range(len(nodes)):
			c2 = nodes[d2_i]
			HD = hamming_distance('{:08b}'.format(int(c1)), '{:08b}'.format(int(c2)))
			distances.append((d1_i, d2_i, HD))


	distances = pd.DataFrame(distances, columns=["Code-1", "Code-2", "HD"])
	distances = distances.pivot("Code-1", "Code-2", "HD")

	mask = np.zeros_like(distances)
	mask[np.triu_indices_from(mask, k=1)] = True
	x_ticks, y_ticks = [], []
	for n in nodes:
		tick = n
		for command in commands_to_codes:
			if commands_to_codes[command]==n:
				tick = command
				break
		x_ticks.append(tick)
		y_ticks.append(tick)
	sns.set(font_scale=0.5)
	hm = sns.heatmap(distances, annot=True, linewidths=.5, mask=mask, cmap="YlGnBu", cbar=False, cbar_kws={'ticks': range(10)}, annot_kws={'ha':'center', 'va':'center', 'fontsize': 8}, xticklabels=x_ticks, yticklabels=y_ticks)

	cat_sums = {}
	for cat1 in range(5):
		nodes1 = nodes[0:categories[cat1][1]]
		cat_sum = 0
		for c1, c2 in product(nodes1, nodes1):
			if c1<c2:
				continue
			cat_sum += (1 if hamming_distance('{:08b}'.format(int(c1)), '{:08b}'.format(int(c2)))==2 else 0)
		cat_sums[cat1] = cat_sum
		plt.text(20, 4+2*cat1, "$\Sigma_{HD=2}$ (Cat. $\leq$ %i) = %i" % (cat1, cat_sum), family='monospace', color=colors[cat1], fontsize=12)
		if cat1==4:
			continue
		plt.hlines(categories[cat1][1], 0, categories[cat1][1], colors=colors[cat1], linewidth=2)
		plt.vlines(categories[cat1][1], 0, categories[cat1][1], colors=colors[cat1], linewidth=2)
	plt.title("%s, Option %i/%i - Hamming Distances" % (args.codeBase, perm_index+1, N_permutations))
	fig_path = "%s/hd_%s_opt%i.pdf" % (output_dir, args.codeBase, perm_index)
	plt.savefig(fig_path)
	plt.clf()
	figure_paths[perm_index]["distances"] = fig_path



#prepare latex skeleton for summary
frame_file = open("latex/frame.tex", "r")
frame_template = frame_file.read()
frame_file.close()
frame_text = ""
for perm_index in sorted(figure_paths):
	this_frame_text = frame_template
	this_frame_text=this_frame_text.replace("<PERMUTATIONINDEX>", str(1+perm_index))
	this_frame_text=this_frame_text.replace("<NPERMUTATIONS>", str(N_permutations))
	for fig_index in figure_paths[perm_index]:
		if fig_index == "distances":
			continue
		this_frame_text = this_frame_text.replace("<PATH%i>"%fig_index, figure_paths[perm_index][fig_index])
	this_frame_text = this_frame_text.replace("<DISTANCES>", figure_paths[perm_index]["distances"])

	codes = np.delete(codeBook[perm_index], 0)
	for cat in range(5):
		catcodes = codes[categories[cat][0]:categories[cat][1]].astype(int)
		this_frame_text = this_frame_text.replace("<CAT%i>"%cat, str(sorted(catcodes.tolist())).replace("[","").replace("]",""))

	frame_text+=this_frame_text



main_file = open("latex/main.tex")
main_template = main_file.read()
main_file.close()
main_template = main_template.replace("<CODESUGGESTION>", args.codeBase)
main_template = main_template.replace("<FRAMES>", frame_text)

tmp_file = os.path.abspath("%s/%s.tex"%(output_dir, args.codeBase))
final_file = os.path.abspath("%s/%s.pdf"%(output_dir, args.codeBase))
with open(tmp_file, "w") as texfile:
	texfile.write(main_template)
